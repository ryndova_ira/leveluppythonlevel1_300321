my_str = 'Hello!'
if '!' in my_str:
    print(f'I found it in {my_str}')

if '???' not in my_str:
    print(f'I did not find it in {my_str}')

if "ddd" in my_str and "!" in my_str:
    print("ldld")
