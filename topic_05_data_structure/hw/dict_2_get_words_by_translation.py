"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""